﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Controller : MonoBehaviour
{
    //Contol style reference from GTA V
    GameObject airplaneFire;
    Rigidbody airplaneRB;

    // Start is called before the first frame update
    void Start()
    {
        //Fetch the Rigidbody component you attach from your GameObject
        airplaneRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        ps4Controller();
    }


    public void ps4Controller()
    {
        var ps4Con = Gamepad.current;

        airplaneFire = GameObject.Find("fire");
        var airplaneFireRender = airplaneFire.GetComponent<Renderer>();

        /* buttonWest buttonEast
         * leftShoulder rightShoulder
         * leftTrigger rightTrigger
         * startButton selectButton
         * leftStickButton rightStickButton */

        // May needed:
        // https://docs.unity3d.com/ScriptReference/Space.Self.html
        

        // Move forward and backward
        // 
        float rightTriggerValue = ps4Con.rightTrigger.ReadValue();
        float leftTriggerValue = ps4Con.leftTrigger.ReadValue();
        if (rightTriggerValue > 0.1)
        {
            airplaneRB.velocity = transform.forward * rightTriggerValue * 6;
            airplaneFireRender.material.SetColor("_Color", Color.red);
        }
        else if (leftTriggerValue > 0.1)
        {
            airplaneRB.velocity = -transform.forward * leftTriggerValue * 2;
            airplaneFireRender.material.SetColor("_Color", Color.green);
        }
        else
        {
            airplaneRB.velocity = transform.forward * 0;
            airplaneFireRender.material.SetColor("_Color", Color.gray);
        }

        //Use shoulder from gamepad (R1, L1 / RB, LB)
        // move horizontal
        if (ps4Con.leftShoulder.isPressed)
        {
            transform.Rotate(new Vector3(0, -0.5f, 0));
        }

        if (ps4Con.rightShoulder.isPressed)
        {
            transform.Rotate(new Vector3(0, 0.5f, 0));
        }

        // Read vale from the left stick
        Vector2 moveLeft = ps4Con.leftStick.ReadValue();

        // Upward and downward
        if (moveLeft.y > 0.1 || moveLeft.y < -0.1)
        {
            transform.Rotate(new Vector3(moveLeft.y / 2, 0, 0));
        }

        // side way
        if (moveLeft.x > 0.1 || moveLeft.x < -0.1)
        {
            float inverse;
            inverse = moveLeft.x * -1;
            transform.Rotate(new Vector3(0, 0, inverse / 2));
        }

        //return to center
        if (ps4Con.selectButton.wasPressedThisFrame)
        {
            transform.position = new Vector3(0, 2, 0);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}

