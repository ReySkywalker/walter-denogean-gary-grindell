[Input System docs](https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/Gamepad.html)

## Set up 
1. import Input System
2. Use the __Controller__ inside the Assets > Script


## Gamepad
1. Create a gamepad

## Velocity
1. Need Rigidbody attach from the GameObject
2. Read value from the gamepad's left and right trigger (float)
3. Use transform.forward * value

## Other control
1. Similar to Velocity
2. Read value from the gamepad
3. Use different transform.

## Debug function
- Use __select__ in controller can set the obejct back to 0,0,0

